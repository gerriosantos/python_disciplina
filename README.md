# DISCIPLINA DE PYTHON

Disciplina ministrada pelo Prof. [Alessio Tony](http://lema.ufpb.br/pesquisador/alessio-almeida "Link do Lema") e Prof. [Hilton Ramalho](http://lema.ufpb.br/pesquisador/hilton-ramalho "Link do Lema"). No segundo semestre do ano de 2019.

## Obejetivos Gerais

Além de uma breve introdução ao python3 e a IDLE PyCharm, o curso visa mostrar as principais técnicas de Machine e Learning, com intuito de familiarizar os pesquisadores na área de data science.
