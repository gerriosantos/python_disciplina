import numpy as np
import os

A = np.array([1, 2, 3])
print(A)

w = np.array([[1, 4],
              [2, 5]])
print(w @ np.linalg.inv(w))

print(os.getcwd())

import pandas as pd

df = pd.read_clipboard(';')
print(df.head())


os.chdir('/home/gerrio/PycharmProjects/Python_Disciplina/aula_06')
print(os.getcwd())
lista_arquivos = os.listdir()
print(lista_arquivos)
