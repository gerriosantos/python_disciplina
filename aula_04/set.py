
# Pode-se tbem transformar uma lista em um Set (conjunto).
## O conjunto não aceita repetições --ele garante vetores únicos, sejam textuais oi numéricos.
"""
x = {1, 2, 3, 1}
print(x)


x = {}   ## Reconhece apenas como dicionário e nao como conjunto.

x.add(1)
x.add(2)
x.add(3)
print(x)
y = {2,5}

z = x.union(y)
print(z)
"""



