
"""

lista = [2000, 2004, 2008, 2012]

print(lista[0])
print(lista[1])
print(lista[2])
print(lista[2])

for ano in lista:
    print(ano)

tupla = (2000, 2004, 2008, 2012)

for ano in tupla:
    print(ano)

conjunto = {2000, 2004, 2008, 2012}

for ano in conjunto:
    print(ano)

Dicionario = {'A':2000, 'B': 2004, 'C':2008, 'D': 2012}

for ano in Dicionario:
    print(ano)

Dicionario = {'A':2000, 'B': 2004, 'C':2008, 'D': 2012}

for ano in Dicionario.values():
    print(ano)

# Imprimir os anos referentes as eleições municipais a partir de 1996.

#Crescente
x = range(1996, 2021, 4)
print(list(x))
#Decrescente
x = range(2020, 1995, -4)
print(list(x))

notas = []
print(notas)

for i in range(10):
    notas.append(i)
print(notas)




    # elevar todos os elementos ao qudrado.

k = [1, 2, 4, 5]
y = []

for i in k:
    y.append(i**2)
print(y)


## Compreensão de lista - codigos icam menores e mais eficientes
z = [i**2 for i in k]
print(z)
"""


precos = [2, 3, 7, 2, 4]
qtd = [1, 10, 2, 3, 5]
produto = ['arroz', 'feijão', 'ovos', 'leite', 'carne']

soma = 0

for x in range(len(precos)):
    gasto = precos[x]*qtd[x]
    soma += gasto
    print(f'{produto[x]:10}: {precos[x]:2} x {qtd[x]:2} = {gasto:4}')








