"""
import math
import random

x = 25

print(math.sqrt(x))
print(math.ceil(2.59))
print(math.floor(2.59))

print(random.choice(range(10)) + 1




import emoji

print('Aprender python me deixa ', emoji.emojize)
"""

"""
def linha():
    print('_' * 50)




linha()
print('Aula de python')
linha()



def linha(x):
    print('_' * 50)


## Função tem que ter uma definição e uma impressão -- print retorna texto
        ## print n retorna valor

def soma(x, y):
    print(x+y)

soma(1,2)


### Quando for numeros usar return

def soma(x, y):
    return x + y
soma(1,2)


def media(x, y):
    return (x + y)/2
print(media(10, 20))

"""

## Será que uma função pode ter um argumento lista, um dicionário...

# Função para cálculo de uma média de uma lista de valores.


# Função fatorial

## Função recursiva --resolver tudo em uma linnha

# numero = int(input("Digite um número inteiro: "))
"""
x = 3
def fatorial(x):
    soma = 1
    while x > 1:
        soma *= x
        x -= 1
        return soma
print(fatorial(3))



for i in range(x, 0, -1):
"""

"""
def fatorial(x):
    soma = 1
    for i in range(x, 1, -1):
        soma *= i
    return soma

print(fatorial())



## Fazer um controle inicial


def fatorial(x):
    if x < 0:
        return f"Erro: << {x} >>. Digite um numero inteiro não negativo!"
    soma = 1
    for i in range(x, 1, -1):
        soma *= i
    return soma

print(fatorial(3))



## Função recursiva  -- um novo padrão de escrita

# 3ª forma de fazer a função fatorial


def fatorial(x):
    if x == 0 or x == 1:
        return 1
    elif x < 0:
        return "Erro"
    else:
        return x * fatorial(x - 1)
print(fatorial(5))



# Funções com mais de um parâmetro

def avg(x = [0, 0, 0], imprimir = True):
    soma = 0
    for i in x:
        soma += i
    if imprimir is False:
        return soma / len(x)
    else:
        print(f"A média de {x} é {soma / len(x):.2f}")

media = avg([10, 3, 5, 8], imprimir = False)

print(media)



# Variaveis globais e locais

y = -5

def teste():
    x = 10
    #y = 2
    print(f"x = {x} é local")
    print(f"y = {y} é global")

teste()
print(y)



# Validação de um número entre 0 e 10.
    

def validar_nota(x, minimo = 0, maximo = 10):
    if minimo <= x <= maximo:
        return print(f"{x} é um número válido")
    else:
        return print(f"Erro: {x} é um número inválido")

validar_nota(11)


## Regras de execeções

    #x = int('ABC')

# lopp infinito


while True:
    try:
        x = int(input("Digite um número inteiro: "))
        print(x)
        break
    except Exception:
        print('ErroNúmeroNãoInteiro: número inválido')
        #x = int(input(" Por favor, digite um número inteiro: "))


# Exemplo com finally

# Sempre vai ser definido um resultado --

## FUNÇÃO LAMBDA

a = lambda x: x ** (1 / 2)

print(a(25))

b = lambda x, y: (x + y) / 2

print(b(25, 5))

"""

# MODULOS E PACOTES
# onde todos os arquivos .py poderão ter várias funções


