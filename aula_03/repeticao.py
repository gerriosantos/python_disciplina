

# Python trabalha com linguagem padrão TRÍVIA (método de identação)


"""
x = 10
while 0 < x <=10:
    print(x)
    x -= 1

x = 10

while x >= 0:
    print(x)
    x -= 1


import time
x = 10

while x>=0:
    time.sleep(1)

    if x is 0:
        print('\t\t FELIZ ANO NOVO')
    else:
        print(f'\t\t\t{x:2}')

    x -= 1




# Imprimir apenas números impares -- usuario infom

x = 0
x_1 = int(input("Digite: "))

while x <= x_1:
    if (x % 2) is not 0:
        print(x)
    x += 1



x = 1         # Contador
soma = 0      # Acumulador

while x <= 3:
    nota = float(input("Digite a nota do aluno: "))

    soma += nota    # acumula a cada repetição que a nota for fornecida.

    x += 1

print(f'A média igual {soma/3:.1f}')     # usando o f que se chama F-String...



# Interropendo repetição

 ## a gente vai criar um looping infinito - mas a qualquer momento está disponível a saída.
 # O looping é infinito enquanto houver dados.

        ## Testando o que é a variável x -- se string, numero, float..
#x =10
#isinstance(x, int)

x = 0
soma = 0

while True:
    entrada = input(f'Digite um {n + 1} número ou qualquer letra para sair: ')
    if entrada.isalpha() == True:
        print()

"""

# Fazer uma tabuada de multiplicação de 1 a 10 --> usando while. -- usar repetições aninhadas -- 2 whiles.

tab = 0
while tab <= 10:
    numero = 0
    while numero <= 10:
        print(f'{tab:2d} x {numero:2d} = {tab * numero:>3d}')
        numero += 1
        if numero == 11:
            print("-----------------------")
        tab += 1

# 2d --> 2 casas decimais  -- d refere-se a digitos, no caso para dizer que é um valor inteiro...


