# L = []

"""
x = [10, 2, 3]
print(x)


x = [10, 2, 8, 'Pizza']
print(x)

y = [3, 1,  2, "Refri"]

z = [x, y]

print(z)

z[0][3] = 'Hamburguer'

print(z)

lista = [0, 1, (1, 3), [1, 6], [(1, 3)]]




w = [1000, 1600, 3000, 2000, 1100, 1200]
media = (w[0] + w[1] + w[2] + w[3] + w[4] + w[5])/6

print(f'a média é de R$ {media:.2f}')


w = [1000, 1600, 3000, 2000, 1100, 1200]
n = 0
soma = 0

while n <= 5:
    soma += w[n]
    n += 1

print(f'A média salarial é de R$ {soma/n:.2f}')



### Operações com listas



# exemplo:


x = [1, 2, 4]
i = 0
soma = 0

while i < 3:
    soma += x[i]
    i += 1
soma



## três métodos de inserir elementos em listas
x = []

x.append('A')
print(x)

x.append(['B', 'C'])  # segundo elemento é uma lista
print(x)


x.extend(['D', 'E'])
print(x)

x.insert(0, 'Z')
print(x)



# remoção de lementos

#a = list(range(5))
#print(a)



x = ['A', 'B', 'C', 'D']
x0 = x[:]
del x[1]
print(f'Obj inicial  {x0} \n Obej final {x}')


x0 = x[:]
del x[1]
print(f'Obj inicial  {x0} \n Obej final {x}')


"""

x = [5, 1, 3]

x.sort(reverse=False)
print(x)

# Criar uma nova lista

x = [3, 2, 7, 1]

y = sorted(x, reverse=True)
print(x)
print(y)


