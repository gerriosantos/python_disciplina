# CLASSES

# Classe PAI

class Pessoa:

    # Função construtora

    def __init__(self, nome, idade, sexo):
        self.nome = nome
        self.idade = idade
        self.sexo = sexo

    # Método - alterar o atributo idade da própria classe

    def alterarIdade(self, idade):
        self.idade = idade

    # Método
    def alterarSexo(self, sexo):
        self.sexo = sexo

        # Método - ver atributo sexo

    def verSexo(self):
        return self.sexo

    # Método - ver atributo nome

    def verNome(self):
        return self.nome

    # Método - ver o atributo idade

    def verIdade(self):
        return self.idade

    # Método - celebração de aniversário

    def aniversarioIdade(self):
        self.idade += 1
        return self.idade


x = Pessoa(nome="João da Silva", idade=20, sexo="M")

y = Pessoa(nome="Maria", idade=3, sexo="F")

print(x.verNome())
print(x.verIdade())
print(x.verSexo())

print(y.verNome())
print(y.verIdade())
print(y.verSexo())


class PessoaFisica(Pessoa):

    def __init__(self, CPF, nome, idade, sexo):
        super().__init__(nome, idade, sexo)
        self.CPF = CPF

    # Método próprios da classe PessoaFisica

    def verCPF(self):
        return self.CPF


x = PessoaFisica("0123", "João", 10, "M")

print(x.verCPF())

