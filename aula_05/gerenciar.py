### Criar um arquivo com números de 1 a 100
"""
arquivo = open(file="números.txt", mode='w')
for i in range(1, 101):
    arquivo.write(f'Linha [{i}]\n')
arquivo.close()


### Sem utilizar o close

with open(file="números.txt", mode='r') as arquivo:
    for i in arquivo.readline():
        print(i)
"""

## Apagá-los ou salvá-los em outras pastas..
# os -- Operacional system

"""


import os
os.getcwd()


## Mudando diretório de trabalho
#os.chdir()

#os.getcwd()


## Criando diretório

os.mkdir('Aula05')
"""


